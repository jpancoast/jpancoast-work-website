
# jpancoast.dev "looking for work" website and associated infrastructure code.
I started here: https://github.com/skillsit/terraform-s3-website, ripped it apart, added a whole bunch of stuff (cloudfront, certs, etc.), and re-constituted it looking something more like I would do professionally.