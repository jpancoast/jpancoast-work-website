
#
# Terraform and aws provider configuration
#
terraform {
  required_version = ">= 0.14"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.35"
    }
  }
  
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "jpancoast"

    workspaces {
      name = "jpancoast-work-website-site"
    }
  }
}

provider "aws" {
  region = "us-west-2"
}
