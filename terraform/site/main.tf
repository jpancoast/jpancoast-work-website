
#
# Upload whatever files that have an updated ETag to the S3 bucket
#
resource "aws_s3_object" "files" {
  for_each = var.files

  bucket = data.aws_s3_bucket.selected.bucket

  key          = each.value.key
  source       = each.value.source
  content_type = each.value.content_type

  etag = filemd5(each.value.source)
}
