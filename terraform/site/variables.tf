variable "bucket_name" {
  description = "Name of the S3 bucket."
  type = string
}

variable "files" {
  description = "map of static files to upload to S3."
  type = map(object(
    {
      key          = string
      source       = string
      content_type = string
    }
  ))
}
