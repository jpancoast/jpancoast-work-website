
#
# Some simple locals, not really needed in something this small but it does look a lot nicer
#
locals {
  logs_bucket_name = "${var.bucket_name}-logs"
  s3_origin_id     = "${var.bucket_name}-origin-id"
}

#
# Call the module that creates the S3 bucket
#
module "s3-website" {
  source = "../modules/s3-website"

  bucket_name = var.bucket_name
}

#
# Create the cert for cloudfront
#
module "cert" {
  providers = {
    aws = aws.us-east-1
  }

  source = "../modules/certificate"

  domain_name               = var.domain_name
  purpose                   = var.purpose
  subject_alternative_names = var.subject_alternative_names
}

#
# Create the cloudfront distributation
#
module "cloudfront" {
  source = "../modules/cloudfront"

  logs_bucket_name = local.logs_bucket_name
  s3_origin_id     = local.s3_origin_id

  bucket_regional_domain_name = module.s3-website.bucket_regional_domain_name
  iam_certificate_id          = module.cert.id
}

resource "aws_iam_role" "test_role" {
  name = "test_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}