variable "bucket_name" {
  type        = string
  description = "Name of the S3 bucket to create."
}

variable "subject_alternative_names" {
  description = "Alternative names for the certificate."
  type        = list(any)
}

variable "domain_name" {
  description = "primary domain name for the cert."
  type        = string
}

variable "purpose" {
  description = "Purpose for the certificate."
  type        = string
}
