
output "website_endpoint" {
  value       = module.s3-website.website_endpoint
  description = "S3 endpoint"
}

output "dvo" {
  value       = module.cert.dvo
  description = "Domain Validation information"
}

output "cloudfront_fqdn" {
  value       = module.cloudfront.fqdn
  description = "Cloudfront FQDN"
}
