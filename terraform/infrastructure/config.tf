
#
# Is it ironic that the terraform block is probably the biggest block in all this code?
#   Note: I have not actually counted lines, but back in my day the terraform block was 
#   like 4 lines
#
terraform {
  required_version = ">= 0.14"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.35"
    }
  }

  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "jpancoast"

    workspaces {
      name = "jpancoast-work-website-infrastructure"
    }
  }
}

#
# I PREFER to do my AWS work in any region other than us-east-1
#
provider "aws" {
  region = "us-west-2"
}

#
# However, the cert needs to be in us-east-1 to work with cloudfront
#
provider "aws" {
  region = "us-east-1"
  alias  = "us-east-1"
}
