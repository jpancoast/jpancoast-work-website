output "fqdn" {
  value       = aws_cloudfront_distribution.s3_distribution.domain_name
  description = "S3 domain name"
}
