variable "logs_bucket_name" {
  description = "Name of the S3 bucket for logs."
  type        = string
}

variable "bucket_regional_domain_name" {
  description = "Bucket regional domain name"
  type        = string
}

variable "s3_origin_id" {
  description = "Origin ID"
  type        = string
}

variable "iam_certificate_id" {
  description = "IAM Certificate ID."
  type        = string
}
