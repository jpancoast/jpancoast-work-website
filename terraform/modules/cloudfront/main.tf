
#
# I'll be honest with ya, I'm not sure what this is for, but I think it's necessary!
#
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "Some comment"
}

#
# Create bucket for cloudfront logs
#   Maybe I should figure out how to visualize the logs...
#
resource "aws_s3_bucket" "logs" {
  bucket = var.logs_bucket_name
}

resource "aws_s3_bucket_acl" "logs" {
  bucket = aws_s3_bucket.logs.id
  acl    = "private"
}

#
# Create the cloudfront distribution
#
resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = var.bucket_regional_domain_name
    origin_id   = var.s3_origin_id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Some comment"
  default_root_object = "index.html"

  logging_config {
    include_cookies = false
    bucket          = aws_s3_bucket.logs.bucket_domain_name
    prefix          = "worksite"
  }

  aliases = ["jpancoast.dev", "www.jpancoast.dev"]

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = var.s3_origin_id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    purpose = "jpancoast work website"
  }

  viewer_certificate {
    acm_certificate_arn = var.iam_certificate_id
    ssl_support_method  = "sni-only"
  }
}
