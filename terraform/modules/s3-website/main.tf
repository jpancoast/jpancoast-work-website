
#
# Create the s3 bucket
#
resource "aws_s3_bucket" "static_site" {
  bucket = var.bucket_name
}

resource "aws_s3_bucket_acl" "static_site" {
  bucket = aws_s3_bucket.static_site.id
  acl    = "public-read"
}

resource "aws_s3_bucket_website_configuration" "static_site" {
  bucket = aws_s3_bucket.static_site.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}
resource "aws_s3_bucket_versioning" "versioning_example" {
  bucket = aws_s3_bucket.static_site.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_policy" "allow_public_get_object_access" {
  bucket = aws_s3_bucket.static_site.id
  policy = data.aws_iam_policy_document.allow_public_get_object_access.json
}

data "aws_iam_policy_document" "allow_public_get_object_access" {
  statement {
    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions = [
      "s3:GetObject"
    ]

    resources = [
      aws_s3_bucket.static_site.arn,
      "${aws_s3_bucket.static_site.arn}/*",
    ]
  }
}

#{
#    "Version": "2012-10-17",
#    "Statement": [
#        {
#            "Sid": "PublicReadGetObject",
#            "Effect": "Allow",
#            "Principal": "*",
#            "Action": [
#                "s3:GetObject"
#            ],
#            "Resource": [
#                "arn:aws:s3:::Bucket-Name/*"
#            ]
#        }
#    ]
#}
