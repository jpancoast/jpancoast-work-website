output "website_endpoint" {
  description = "The public url of this website."
  value       = aws_s3_bucket_website_configuration.static_site
}

output "bucket_regional_domain_name" {
  description = "Bucket Regional Domain Name"
  value       = aws_s3_bucket.static_site.bucket_regional_domain_name
}
