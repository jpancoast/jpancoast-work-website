
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.35"
    }
  }
}

#
# This just creates a simple Cert, signed by AWS.
#
resource "aws_acm_certificate" "cert" {
  domain_name               = var.domain_name
  validation_method         = var.validation_method
  subject_alternative_names = var.subject_alternative_names

  tags = {
    purpose = var.purpose
  }

  lifecycle {
    create_before_destroy = true
  }
}
