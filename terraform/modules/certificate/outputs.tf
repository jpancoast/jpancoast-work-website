output "dvo" {
  value       = aws_acm_certificate.cert.domain_validation_options
  description = "Domain Validation Options"
}

output "id" {
  value       = aws_acm_certificate.cert.id
  description = "cert ID"
}
