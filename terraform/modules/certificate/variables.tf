variable "domain_name" {
  description = "primary domain name for the cert."
  type        = string
}

variable "validation_method" {
  description = "How to validate the cert."
  default     = "DNS"
  type        = string
}

variable "purpose" {
  description = "Purpose for the certificate."
  type        = string
}

variable "subject_alternative_names" {
  description = "Alternative names for the certificate."
  type        = list(any)
}
